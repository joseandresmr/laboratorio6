package cql.ecci.ucr.ac.laboratorio6;

import android.app.Fragment;
import android.os.Bundle;



import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import cql.ecci.ucr.ac.laboratorio6.datos.Persona;


public class PersonaFragmento extends Fragment
{
    private static final String NAME_PARAM = "param1";

    // TODO: Rename and change types of parameters
    private Persona persona;

    public PersonaFragmento()
    {
        // Required empty public constructor
    }

    public static PersonaFragmento newInstance(Persona tableTopParam) {
        PersonaFragmento fragment = new PersonaFragmento();
        Bundle args = new Bundle();
        args.putParcelable(NAME_PARAM, tableTopParam);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            persona = getArguments().getParcelable(NAME_PARAM);
        }
    }

    public Persona getPersona()
    {
        return persona;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Must inflate before setting the texts
        View view = inflater.inflate(R.layout.fragment_persona, container,false);

        TextView nombre = view.findViewById(R.id.nombre);
        nombre.setText(persona.getNombre());

        TextView identificacion = view.findViewById(R.id.identificacion);
        identificacion.setText(persona.getIdentificacion());

        ImageView imagen = view.findViewById(R.id.imagen);
        imagen.setImageResource(persona.getIdImagen());



        return view;
    }
}
