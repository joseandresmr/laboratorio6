package cql.ecci.ucr.ac.laboratorio6.datos;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Persona.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase
{
    public abstract PersonaDao personaDao();
}