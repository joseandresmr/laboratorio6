package cql.ecci.ucr.ac.laboratorio6.datos;

public class BaseDataItemsException extends Exception {
    public BaseDataItemsException(String msg) {
        super(msg);
    }
}