package cql.ecci.ucr.ac.laboratorio6.datos;

public class CantRetrieveItemsException extends Exception
{
    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}