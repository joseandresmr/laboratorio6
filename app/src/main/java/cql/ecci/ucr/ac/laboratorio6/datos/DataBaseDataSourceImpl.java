package cql.ecci.ucr.ac.laboratorio6.datos;

import android.content.Context;
import android.os.AsyncTask;

import androidx.room.Room;

import java.util.ArrayList;
import java.util.List;

import cql.ecci.ucr.ac.laboratorio6.presentacion.TareaAsync;

// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public class DataBaseDataSourceImpl implements DataBaseDataSource
{
    private Context context;
    private AppDatabase db;
    private TareaAsync tareaAsync;
    private Object syncToken;


    public DataBaseDataSourceImpl(final Context context)
    {
        this.context = context;
        db = Room.databaseBuilder(context,  AppDatabase.class, "database-name").build();

        syncToken = new Object();
        tareaAsync = new TareaAsync(syncToken, db);
        tareaAsync.start();
    }

    @Override
    public List<Persona> obtainItems() throws BaseDataItemsException
    {
        List<Persona> items = null;
        try
        {
            synchronized (syncToken)
            {
                try
                {
                    syncToken.wait();
                    items = tareaAsync.obtenerPersonas();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e)
        {
            throw new BaseDataItemsException(e.getMessage());
        }
        return items;
    }
}