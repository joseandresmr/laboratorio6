package cql.ecci.ucr.ac.laboratorio6.datos;

import java.util.List;
// Capa de datos (Model)
// El repositorio decide de que fuente de datos obtiene los valroes

// Capa de datos (Model)
// El repositorio decide de que fuente de datos obtiene los valroes
public interface ItemsRepository
{
    List<Persona> obtainItems() throws CantRetrieveItemsException;
}