package cql.ecci.ucr.ac.laboratorio6.datos;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Persona implements Parcelable
{
    @PrimaryKey
    @NonNull
    private String identificacion;

    @ColumnInfo(name = "nombre")
    private String nombre;

    @ColumnInfo(name = "idImagen")
    private int idImagen;

    public static final String key = "Persona";

    public Persona(String identificacion, String nombre, int idImagen)
    {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.idImagen = idImagen;
    }

    public Persona(Parcel in)
    {
        this.identificacion = in.readString();
        this.nombre = in.readString();
        this.idImagen = in.readInt();
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public int getIdImagen() {
        return idImagen;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(identificacion);
        dest.writeString(nombre);
        dest.writeInt(idImagen);
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>()
    {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }
        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };
}