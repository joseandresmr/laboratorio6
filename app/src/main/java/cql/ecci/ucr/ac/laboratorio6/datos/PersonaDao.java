package cql.ecci.ucr.ac.laboratorio6.datos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PersonaDao
{
    @Query("SELECT * FROM persona")
    List<Persona> getAll();

    @Insert
    void insertAll(Persona... personas);

    @Query("DELETE FROM persona")
    void deleteAll();
}
