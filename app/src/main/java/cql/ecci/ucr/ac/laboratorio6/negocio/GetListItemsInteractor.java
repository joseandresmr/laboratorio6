package cql.ecci.ucr.ac.laboratorio6.negocio;

import java.util.List;

import cql.ecci.ucr.ac.laboratorio6.datos.Persona;

public interface GetListItemsInteractor
{
    interface OnFinishedListener {
        void onFinished(List<Persona> items);
    }
    void getItems(OnFinishedListener listener);
}
