package cql.ecci.ucr.ac.laboratorio6.negocio;

import android.content.Context;

import java.util.List;

import cql.ecci.ucr.ac.laboratorio6.datos.Persona;
import cql.ecci.ucr.ac.laboratorio6.datos.CantRetrieveItemsException;
import cql.ecci.ucr.ac.laboratorio6.datos.ItemsRepository;
import cql.ecci.ucr.ac.laboratorio6.datos.ItemsRepositoryImpl;

// Capa de Negocios (Presenter o Controller)
// Implementacion de GetListItemsInteractor de la capa de negocio (P o M) para obtener los resultados de la lista de elementos a mostrar
// Representa el Interactor (casos de uso), se comunica con las entidades y el presenter
public class GetListItemsInteractorImpl implements GetListItemsInteractor
{
    private ItemsRepository mItemsRepository;

    private Context context;

    public GetListItemsInteractorImpl(Context context)
    {
        this.context = context;
    }

    @Override public void getItems(final OnFinishedListener listener)
    {
        List<Persona> items = null;
        mItemsRepository = new ItemsRepositoryImpl(context);
        try
        {
            // obtenemos los items
            items = mItemsRepository.obtainItems();
        }
        catch (CantRetrieveItemsException e)
        {
            e.printStackTrace();
        }
        // Al finalizar retornamos los items
        listener.onFinished(items);
    }
}