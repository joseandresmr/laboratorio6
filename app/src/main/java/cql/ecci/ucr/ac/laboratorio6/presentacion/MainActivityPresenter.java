package cql.ecci.ucr.ac.laboratorio6.presentacion;

public interface MainActivityPresenter
{
    // resumir
    void onResume();
    // evento cuando se hace clic en la lista de elementos
    void onItemClicked(int position);
    // destruir
    void onDestroy();

}
