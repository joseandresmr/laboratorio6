package cql.ecci.ucr.ac.laboratorio6.presentacion;

import android.content.Context;

import androidx.room.Room;

import java.util.ArrayList;
import java.util.List;

import cql.ecci.ucr.ac.laboratorio6.R;
import cql.ecci.ucr.ac.laboratorio6.datos.AppDatabase;
import cql.ecci.ucr.ac.laboratorio6.datos.Persona;

public class TareaAsync extends  Thread
{
    Object syncToken;

    AppDatabase db;

    List<Persona> lista;

    public TareaAsync(Object syncToken, AppDatabase db)
    {
        this.syncToken = syncToken;
        this.db = db;
        this.lista = new ArrayList<>();
    }


    @Override
    public void run()
    {
        synchronized (syncToken)
        {
            db.personaDao().deleteAll();
            db.personaDao().insertAll(
                    new Persona("0000", "Juan Hernández", R.drawable.calamardo),
                    new Persona("0001", "Jennifer Vargas", R.drawable.esponja),
                    new Persona("0002", "Octavio Jara", R.drawable.patricio),
                    new Persona("0003", "Gloriana Villalobos", R.drawable.cangrejo));

            lista = db.personaDao().getAll();
            syncToken.notify();
        }
    }

    public List<Persona> obtenerPersonas()
    {
        return lista;
    }

}
